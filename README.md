You can try this app allready runned here: https://newzmatetest-nchudnovets.c9users.io/
and not to setup it locally
At first time you can see an warning at orange screen, then press Open the App button. 

Admins username/password are: admin/123456
other users have usernames the same as lastName (i.e. User_0), and password: 123456

If you want to setup progect locally:
1. Install Meteor according to instructions here: https://www.meteor.com/install
2. inside project's directory run:   meteor npm install --save
3. inside project's directory run:   meteor

Admin user will be created at startup if it doesn't exists.

Only admin can add/register new users.

All working app's code is inside /imports folder. Folders /client and /server in the root are just for import from /imports
That's a little Meteor specific.
