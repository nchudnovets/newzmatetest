import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';


Meteor.users.attachSchema( new SimpleSchema({
  _id: {
    type: String,
    regEx: SimpleSchema.RegEx.Id
  },
  createdAt:{
    type: Date,
    autoValue: function(){
      if (this.isInsert) {
          return new Date;
      }
    },
     index: -1
  },
  username: {
   type: String,
   index: 1
  },
  firstName: {
    type: String,
    index: 1
  },
  lastName: {
    type: String,
    index: 1
  },
  role: {
    type: String,
    allowedValues: ['admin', 'boss', 'user'],
    index: 1
  },
  bossId: {
    type: String,
    optional: true
  },
  subordinatesIds: {
    type: [String],
    optional: true
  },
  services:{
    type: Object,
    optional: true,
    blackbox: true
  }
  
}));

