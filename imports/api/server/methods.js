const addToSubordinates = function(bossId, subId){
  const user = Meteor.users.findOne({_id: bossId});
  const modifier = {
    $addToSet: {subordinatesIds: subId}
  };
  if(user.role === 'user'){
    modifier.$set = {role: 'boss'};
  }
  Meteor.users.update({_id: bossId}, modifier);
};

//we don't use removeFromSubordinates now, because according to cuurent conditions
//only boss can reassign suborinates and only for its own suborinates
//so actually now there is no situation when we remove suborinate from list
//function is written for fufture cases

// const removeFromSubordinates = function(prevBossId, subId){
//   const user = Meteor.users.findOne({_id: prevBossId});
//   let modifier = {
//     $pull: {subordinatesIds: subId}
//   };
//   if(user.subordinatesIds.length<2){
//     modifier = {
//       $set: {role: 'user'},
//       $unset: {subordinatesIds: ''}
//     };
//   }
//   Meteor.users.update({_id: prevBossId}, modifier);
// };

Meteor.methods({
  createNewUser: function(params){
    const { 
      username, 
      password ='123456', 
      firstName, 
      lastName, 
      role='user',
      bossId=this.userId
    } = params;
    let result = {};
    
    const currentUser = Meteor.users.findOne({_id: this.userId});
    if(!currentUser || currentUser.role !== 'admin'){
      console.log("access denied");
      return;
    }
    
    const requires = [username, firstName, lastName];
    const checkRequires = _.every(requires, (item)=>{
      return !!item;
    });
    if(!checkRequires){
      result = {
        success: false,
        reason: "fill all data, please"
      };
      return result;
    }
    
    try{
      //Meteor makes possible to write async this symple way
      let uid = Accounts.createUser({username, password});
      result = {
        success: true,
        userId: uid
      };
    }
    catch (err){
       result = {
        success: false,
        reason: err.reason
      };
    }
    if(result.success){
      let _id = result.userId;
      let params = {
        role,
        firstName, 
        lastName, 
        bossId
      };
      
      Meteor.users.update({_id}, {$set: params}, (err)=>{
        if(err){
          console.log(`${new Date()}: player default params setting failed: `, err);
        }
        else{
          addToSubordinates(bossId, _id);
        }
      });
    }
    return result;
  },
  
  
  changeUsersBoss: function(params){
    const {userId, bossId} = params;
    
    const currentUser = Meteor.users.findOne({_id: this.userId});
    
    const targetUser = Meteor.users.findOne({_id: userId});
    const newBoss = Meteor.users.findOne({_id: bossId});
    
    if( !targetUser || !currentUser || !newBoss || userId===bossId || targetUser.bossId===bossId ){
      return;
    }
    
    if(!this.userId || currentUser._id !== targetUser.bossId){
      console.log("access denied");
      return;
    }
    
    addToSubordinates(bossId, userId);
    Meteor.users.update({_id: userId},{$set: {bossId}});
  },
  
  
  getData: function(){
    const user = Meteor.users.findOne({_id: this.userId});
    if(!user){return}
    
    const userRole = user.role || 'user';
    const defineQuery = ()=>{
      switch (userRole) {
        case 'admin':
          return {};
        case 'boss': 
          return { _id: {$in: user.subordinatesIds} };
        default:
          return { _id: this.userId };
      }
    };
    return Meteor.users.find(defineQuery(), {sort: {createdAt: -1}}).fetch();
  }
  
  
});


