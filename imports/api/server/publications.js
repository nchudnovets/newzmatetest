Meteor.publish("user", function(){
  if (this.userId) {
    return Meteor.users.find({_id: this.userId},
                             {fields: {
                               _id: 1,
                               firstName: 1,
                               lastName: 1,
                               role: 1,
                               bossId: 1,
                               subordinatesIds: 1,
                            }});
  } else {
    this.ready();
  }
});
