import { FlowRouter } from 'meteor/kadira:flow-router';
import React from 'react';
import { mount } from 'react-mounter';
import { PageLayout } from '/imports/ui/layout/PageLayout.js';
import { HomePageContainer } from '/imports/ui/homepage/HomePageContainer.js';
import { LoginPage } from '/imports/ui/login/LoginPage.js';

const checkLogged = function(context, redirect){
  if( (!Meteor.userId()) && (context.route.name !== 'login') ){
      redirect('/login');
  }
};


FlowRouter.route('/', {
  name: 'home',
  action() {
    mount(
      PageLayout, { content: <HomePageContainer /> }
    );
  },
  triggersEnter: [checkLogged]
});


FlowRouter.route('/login', {
  name: 'login',
  action() {
    mount(
      PageLayout, { content: <LoginPage /> }
    );
  }
});