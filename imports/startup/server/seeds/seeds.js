
Meteor.startup(()=>{
  if(Meteor.users.find({role: "admin"}).count()<1){
    console.log("creating admin at startup");
    const _id = Accounts.createUser({
      username: `admin`, 
      password: "123456"
    });
    if(!!_id){
      const params = {
        role: 'admin',
        firstName: "Ado",
        lastName: "Adminus"
      };
      Meteor.users.update({_id}, {
        $set: params
      },
        (err)=>{
          if(err){
            console.log(`${new Date()}: admin default params setting failed: `, err);
          }
        }
      );
    }
    
  }
  
});