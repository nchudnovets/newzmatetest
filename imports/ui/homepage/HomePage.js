import React, { Component } from 'react';
import ViewModel from 'viewmodel-react';
import { FlowRouter } from 'meteor/kadira:flow-router';

HomePage({
  created(){
    this.getData();
  },
  
  loading: false, 
  
  userList: null,
  
  getData(){
    this.loading(true);
    Meteor.call("getData", (err, res)=>{
      if(err){
        alert("can not get data");
      }
      else{
        this.userList(res);
      }
      this.loading(false);
    });
  },
  
  createUser(event){
    event.preventDefault();
    const username = this.refs.username.value;
    const firstName = this.refs.firstName.value;
    const lastName = this.refs.lastName.value;
    
    Meteor.call('createNewUser', {username, firstName, lastName}, (err, result)=>{
      if(err){
        console.log(err);
        alert("received err!", err.message);
      }
      else if(result.success){
        alert('New user created');
        this.getData();
      }
      else if(!result.success){
        alert(`Attention! ${result.reason}`);
      }
    });
  },
  
  getUserName(userId){
    const user = _.findWhere(_.toArray(this.userList()).concat(Meteor.user()), {_id: userId});
    return `${user.firstName} ${user.lastName}`;
  },
  
  changeUsersBoss({userId, bossId}){
    Meteor.call("changeUsersBoss", {userId, bossId}, (err, res)=>{
      if(err){
        alert("can not make changes");
      }
      else{
        this.getData();
      }
    });
  },
  
  renderUserRow(userId){
    const user = _.findWhere(this.userList(), {_id: userId});
    return(
      <li key={userId} style={{marginLeft: '10px'}}>
        <p>{ user.firstName } {user.lastName}</p>
        {
          !!user.bossId
          ?
            <p>
              Current user's boss is {this.getUserName(user.bossId)}
            </p>
          :
            null
        }
        {
          user.bossId === Meteor.userId()
          ?
            <p>Set as new boss:
              {
                this.userList().map((colleague)=>{
                  return (
                    colleague._id === user.bossId || colleague._id === user._id
                    ? 
                      null 
                    : 
                      <button 
                        key={colleague._id} 
                        onClick={ ()=> this.changeUsersBoss({userId, bossId:colleague._id})}
                      >
                        {this.getUserName(colleague._id)}
                      </button>
                  );
                })
              }
            </p>
          :
            null
        }
            
      </li>
    );
  },
  
  logout(){
    FlowRouter.go('/login');
    Meteor.logout();
  },
  
  render() {
    return (
        this.loading()
        ?
          <div>loading data...</div>
        :
          <div>
            <div>
              <button b="click: logout">Logout</button>
            </div>
            {
              Meteor.user().role === 'admin'
              ?
                <div>Create New user: 
                  <form onSubmit={this.createUser}>
                    <div>
                      username: 
                      <input 
                        type="text" 
                        ref="username"
                      />
                    </div>
                        
                    <div>
                      firstName
                      <input 
                        type="text"
                        ref="firstName"
                      />
                    </div>
                    
                    <div>
                      lastName
                      <input 
                        type="text"
                        ref="lastName"
                      />
                    </div>
                    <div>
                      <button type="submit" >Create</button>
                    </div>
                  </form>  
                </div>
              :
                null
            }
            <ul>List of users:
              <li>
                {Meteor.user().firstName} {Meteor.user().lastName}
              </li>
              {
                !!Meteor.user().subordinatesIds && Meteor.user().subordinatesIds.length>0
                ?
                  Meteor.user().subordinatesIds.map((userId)=>{
                    return (
                      this.renderUserRow(userId)
                    );
                  })
                :
                  null
              }
              
            </ul>
          </div>
    );
  }
});

export { HomePage };
