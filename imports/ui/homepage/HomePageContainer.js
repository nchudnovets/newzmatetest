import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { HomePage } from './HomePage';

const HomePageContainer = createContainer((incomeParams) => {
  
  const dataHandle = Meteor.subscribe('user');
  const user = Meteor.users.findOne({_id: Meteor.userId()});
  
  return {
    user
  };
}, HomePage);

export { HomePageContainer };