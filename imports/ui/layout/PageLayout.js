import React from 'react';

const PageLayout = ({ content }) => (
  <div className = "page_wrap">
    {content}
  </div>
);

export { PageLayout };