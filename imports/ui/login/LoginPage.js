import React, { Component } from 'react';
import ViewModel from 'viewmodel-react';
import { FlowRouter } from 'meteor/kadira:flow-router';


LoginPage({
  
  _sendData(event){
    event.preventDefault();
    const username = this.refs.username.value.toLowerCase().trim();
    const password = this.refs.pwd.value;
    const requires = [username, password];
    const checkRequires = _.every(requires, (item)=>{
      return !!item;
    });
    
    if(!checkRequires){
      alert("Fill data, please");
      return;
    }
    Meteor.loginWithPassword(username, password, (Error)=>{
      if(Error){
        console.log(Error);
        if(Error.error===403){
          alert("user not found");
        }
      }
      else{
        FlowRouter.go("/");
      }
    });
  },
  
  render(){
    return (
      <form onSubmit={this._sendData}>
        <div>
          username: 
          <input 
            type="text" 
            ref="username"
          />
        </div>
           
        <div>
          Password: 
          <input 
            type="password"
            ref="pwd"
          />
        </div>
        
        <div>
          <button type="submit">Log In</button>
        </div>
      </form>  
    );
  }
});

export { LoginPage };